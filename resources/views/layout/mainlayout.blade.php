<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>TELL TERA</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ URL::asset('/assets/img/favicon.png')}}">
    
    @include('layout.partials.head')
  </head>
   @if(Route::is(['error-404','error-500']))
   <body class="error-page">
     @endif
    <body class="nk-body bg-lighter npc-default has-sidebar no-touch nk-nio-theme">
        @if(!Route::is(['error-404','error-500']))
        <div class="main-wrapper">
            @include('layout.partials.header')
            @include('layout.partials.sidebar')
            <div class="page-wrapper">
                <div class="content container-fluid">
                    @if(session()->has('success'))
                      <div class="alert alert-success">
                          {{ session()->get('success') }}
                      </div>
                    @endif
                    @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                    @endif
                    @yield('content')
                </div>
            </div>
        </div>
        @endif
        @if(Route::is(['error-404','error-500']))
            @yield('content')
        @endif
     
 <!-- /Main Wrapper -->
    @include('layout.partials.footer-scripts')
  </body>
</html>
 