<div class="sidebar" id="sidebar">
				<div class="sidebar-inner slimscroll">
					<div id="sidebar-menu" class="sidebar-menu">
						<ul>
							<li class="menu-title"><span>Main</span></li>
							<li @if(strpos(Route::current()->uri, 'index') !== false) class="active" @endif>
								<a href="/index"><i data-feather="home"></i> <span>Dashboard</span></a>
							</li>
							@if(Auth::user()->role=='admin')
							<li @if(strpos(Route::current()->type, 'client') !== false) class="active" @endif>
								<a href="/user/wholesale_client/list"><i data-feather="file-text"></i> <span>Wholesale Clients</span></a>
							</li>
							<li  @if(strpos(Route::current()->type, 'dealer') !== false) class="active" @endif>
								<a href="/user/dealer/list"><i data-feather="star"></i> <span>Dealers</span></a>
							</li>
							<li class="submenu">
								<a href="#" @if(strpos(Route::current()->uri, 'service/category') !== false || strpos(Route::current()->uri, 'service/list') !== false) class="subdrop" @endif ><i data-feather="clipboard"></i> <span> Services</span> <span class="menu-arrow"></span></a>
								<ul @if(strpos(Route::current()->uri, 'service/category') !== false || strpos(Route::current()->uri, 'service/list') !== false) style="display:block" @endif>
									<li  @if(strpos(Route::current()->uri, 'service/category') !== false) class="active" @endif ><a href="/service/category/list">Services Categories</a></li>
									<li  @if(strpos(Route::current()->uri, 'service/list') !== false) class="active" @endif ><a href="/service/list">Services</a></li>
									
								</ul>
							</li>
							@endif
							@if(Auth::user()->role=='wholesale_client')
							<li @if(strpos(Route::current()->uri, 'customer/add') !== false) class="active" @endif>
								<a href="/customer/add"><i data-feather="users"></i> <span>@if(Auth::user()->role=='dealer') Add New Customer @else  Activate New Service @endif</span></a>
							</li>
							@if(Auth::user()->payment_method=='offline')
							<li @if(strpos(Route::current()->uri, 'invoice') !== false) class="active" @endif>
								<a href="/invoice/list"><i data-feather="users"></i> <span>Invoices</span></a>
							</li>
							<li @if(strpos(Route::current()->uri, 'recurring') !== false) class="active" @endif>
								<a href="/recurring"><i data-feather="users"></i> <span>Recurring</span></a>
							</li>
							<li @if(strpos(Route::current()->uri, 'payment/history') !== false) class="active" @endif>
								<a href="/payment/history"><i data-feather="users"></i> <span>Payment History</span></a>
							</li>
							@endif
							@endif
							<!--<li>-->
							<!--	<a href="customers.html"><i data-feather="users"></i> <span>Open Orders</span></a>-->
							<!--</li>-->
							@if(Auth::user()->payment_method=='online')
							<li @if(strpos(Route::current()->uri, 'customer/list') !== false) class="active" @endif>
								<a href="/customer/list"><i data-feather="users"></i> <span>Customers</span></a>
							</li>
						@endif
							@if(Auth::user()->role=='admin')
							<li @if(strpos(Route::current()->uri, 'invoice') !== false) class="active" @endif>
								<a href="/activation/requests"><i data-feather="users"></i> <span>Activation Request</span></a>
							</li>
							
							<li class="submenu">
								<a href="#"><i data-feather="clipboard"></i> <span> Invoices</span> <span class="menu-arrow"></span></a>
								<ul>
									<li><a href="invoices.html">Invoices List</a></li>
									<li><a href="invoice-grid.html">Invoices Grid</a></li>
									<li><a href="invoice-grid-two.html">Invoices Grid 2</a></li>
									<li><a href="add-invoice.html">Add Invoices</a></li>
									<li><a href="edit-invoice.html">Edit Invoices</a></li>
									<li><a href="view-invoice.html">Invoices Details</a></li>
									<li><a href="view-invoice-two.html">Invoices Details 2</a></li>
									<li><a href="invoices-settings">Invoices Settings</a></li>
								</ul>
							</li>
							
							@endif
							@if(Auth::user()->role=='admin' || (Auth::user()->role=='wholesale_client' && Auth::user()->payment_method=='online'))
							<li @if(strpos(Route::current()->uri, 'ticket') !== false) class="active" @endif>
								<a href="/ticket/list" ><i data-feather="credit-card"></i> <span>Tickets</span>
								@if(Auth::user()->role=='admin' && Session::get('tickets'))
								<span class="badge badge-pill badge-danger badge-up cart-item-count1">{{Session::get('tickets')}}</span>
								@endif
								</a>
							</li>
							@endif
							@if(Auth::user()->role=='admin')
							<li>
								<a href="/category/list"><i data-feather="package"></i> <span>Categories</span></a>
							</li>
							<!--<li>-->
							<!--	<a href="/service/category/list"><i data-feather="settings"></i> <span>Services Categories</span></a>-->
							<!--</li>-->
							<!--<li>-->
							<!--	<a href="settings.html"><i data-feather="settings"></i> <span>Services</span></a>-->
							<!--</li>-->
							
							<li>
								<a href="/settings"><i data-feather="settings"></i> <span>Settings</span></a>
							</li>
							@endif
							<!--<li class="submenu">-->
							<!--	<a href="#"><i data-feather="pie-chart"></i> <span> Reports</span> <span class="menu-arrow"></span></a>-->
							<!--	<ul>-->
							<!--		<li><a href="sales-report.html">Sales Report</a></li>-->
							<!--		<li><a href="expenses-report.html">Expenses Report</a></li>-->
							<!--		<li><a href="profit-loss-report.html">Profit & Loss Report</a></li>-->
							<!--		<li><a href="taxs-report.html">Taxs Report</a></li>-->
							<!--	</ul>-->
							<!--</li>-->
								<li><a href="/client/login/logout/true"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg> <span>Logout</span></a></li>
							
						</ul>
					</div>
				</div>
			</div>