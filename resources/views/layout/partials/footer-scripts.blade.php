<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://jstest.authorize.net/v3/AcceptUI.js" charset="utf-8">
</script>
<script src="/assets/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/feather.min.js"></script>
<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/datatables/datatables.min.js"></script>
<script src="/assets/plugins/moment/moment.min.js"></script>
<script src="/assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="/assets/plugins/apexchart/apexcharts.min.js"></script>
<script src="/assets/plugins/apexchart/chart-data.js"></script>
<script src="/assets/js/jquery-ui.min.js"></script>
<script src="/assets/js/script.js"></script>

@yield('script')