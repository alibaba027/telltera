<link rel="shortcut icon" href="/assets/img/favicon.png">
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/plugins/fontawesome/css/fontawesome.min.css">
<link rel="stylesheet" href="/assets/plugins/fontawesome/css/all.min.css">
<link rel="stylesheet" href="/assets/plugins/feather/feather.css">
<link rel="stylesheet" href="/assets/plugins/datatables/datatables.min.css">
<link rel="stylesheet" href="/assets/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" href="/assets/css/style.css">
@yield('css')