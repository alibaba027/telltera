@extends('layout.mainlayout')
@section('content')
		
				<div class="content container-fluid">
                    @if(Auth::user()->role=='admin')
					<div class="row">
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon bg-1">
											<i class="fas fa-dollar-sign"></i>
										</span>
										<div class="dash-count">
											<div class="dash-title">Total Revenue</div>
											<div class="dash-counts">
												<p>0</p>
											</div>
										</div>
									</div>
									<div class="progress progress-sm mt-3">
										<div class="progress-bar bg-5" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon bg-2">
											<i class="fas fa-users"></i>
										</span>
										<div class="dash-count">
											<div class="dash-title">Customers</div>
											<div class="dash-counts">
												<p>0</p>
											</div>
										</div>
									</div>
									<div class="progress progress-sm mt-3">
										<div class="progress-bar bg-6" role="progressbar" style="width:100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon bg-3">
											<i class="fas fa-file-alt"></i>
										</span>
										<div class="dash-count">
											<div class="dash-title">Total Invoice Amount</div>
											<div class="dash-counts">
												<p>0</p>
											</div>
										</div>
									</div>
									<div class="progress progress-sm mt-3">
										<div class="progress-bar bg-7" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon bg-4">
											<i class="far fa-file"></i>
										</span> 
										<div class="dash-count">
											<div class="dash-title">Projected Profit</div>
											<div class="dash-counts">
												<p>0</p>
											</div>
										</div>
									</div>
									<div class="progress progress-sm mt-3">
										<div class="progress-bar bg-8" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endif
					@if(Auth::user()->role=='wholesale_client')
					<div class="row">
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon bg-1">
											<i class="fas fa-dollar-sign"></i>
										</span>
										<div class="dash-count">
											<div class="dash-title">Today $</div>
											<div class="dash-counts">
												<p>{{$today_revenue??0}}</p>
											</div>
										</div>
									</div>
									<div class="progress progress-sm mt-3">
										<div class="progress-bar bg-5" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon bg-2">
											<i class="fas fa-users"></i>
										</span>
										<div class="dash-count">
											<div class="dash-title">Customers</div>
											<div class="dash-counts">
												<p>{{$total_customers??0}}</p>
											</div>
										</div>
									</div>
									<div class="progress progress-sm mt-3">
										<div class="progress-bar bg-6" role="progressbar" style="width:100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
									
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon bg-3">
											<i class="fas fa-file-alt"></i>
										</span>
										<div class="dash-count">
											<div class="dash-title">Total $</div>
											<div class="dash-counts">
												<p>{{$total_revenue??0}}</p>
											</div>
										</div>
									</div>
									<div class="progress progress-sm mt-3">
										<div class="progress-bar bg-7" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								
								</div>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12">
							<div class="card">
								<div class="card-body">
									<div class="dash-widget-header">
										<span class="dash-widget-icon bg-4">
											<i class="far fa-file"></i>
										</span> 
										<div class="dash-count">
											<div class="dash-title">Total Tickets</div>
											<div class="dash-counts">
												<p>{{$tickets??0}}</p>
											</div>
										</div>
									</div>
									<div class="progress progress-sm mt-3">
										<div class="progress-bar bg-8" role="progressbar" style="width: 100%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endif
					<div class="row">
						<div class="col-xl-12 d-flex">
							<div class="card flex-fill">
								<div class="card-header">
									<div class="d-flex justify-content-between align-items-center">
										<h5 class="card-title">Sales Analytics</h5>

										<div class="dropdown">
											<button class="btn btn-white btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
												Monthly
											</button>
											<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<li>
													<a href="javascript:void(0);" class="dropdown-item">Weekly</a>
												</li>
												<li>
													<a href="javascript:void(0);" class="dropdown-item">Monthly</a>
												</li>
												<li>
													<a href="javascript:void(0);" class="dropdown-item">Yearly</a>
												</li>												
											</ul>
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="d-flex align-items-center justify-content-between flex-wrap flex-md-nowrap row">
										<div class="w-md-100 d-flex align-items-center mb-3 flex-wrap flex-md-nowrap row">
											<div class="col-lg-3">
												<span>YTD Expenses</span>
												<p class="h3 text-primary me-5">$0</p>
											</div>
											<div  class="col-lg-3">
												<span>YTD Profit</span>
												<p class="h3 text-success me-5">$0</p>
											</div>
											<div  class="col-lg-3">
												<span>Active Customers</span>
												<p class="h3 text-danger me-5">$0</p>
											</div>
											<div  class="col-lg-3">
												<span>Total Inactive</span>
												<p class="h3 text-dark me-5">$0</p>
											</div>
										</div>
									</div>
									
									<div id="sales_chart"></div>
								</div>
							</div>
						</div>
						<!--<div class="col-xl-5 d-flex">-->
						<!--	<div class="card flex-fill">-->
						<!--		<div class="card-header">-->
						<!--			<div class="d-flex justify-content-between align-items-center">-->
						<!--				<h5 class="card-title">Invoice Analytics</h5> -->
										
						<!--				<div class="dropdown">-->
						<!--					<button class="btn btn-white btn-sm dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">-->
						<!--						Monthly-->
						<!--					</button>-->
						<!--					<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">-->
						<!--						<li>-->
						<!--							<a href="javascript:void(0);" class="dropdown-item">Weekly</a>-->
						<!--						</li>-->
						<!--						<li>-->
						<!--							<a href="javascript:void(0);" class="dropdown-item">Monthly</a>-->
						<!--						</li>-->
						<!--						<li>-->
						<!--							<a href="javascript:void(0);" class="dropdown-item">Yearly</a>-->
						<!--						</li>-->
						<!--					</ul>-->
						<!--				</div>-->
						<!--			</div>-->
						<!--		</div>-->
						<!--		<div class="card-body">-->
						<!--			<div id="invoice_chart"></div>-->
						<!--			<div class="text-center text-muted">-->
						<!--				<div class="row">-->
						<!--					<div class="col-4">-->
						<!--						<div class="mt-4">-->
						<!--							<p class="mb-2 text-truncate"><i class="fas fa-circle text-primary me-1"></i> Invoiced</p>-->
						<!--							<h5>$2,132</h5>-->
						<!--						</div>-->
						<!--					</div>-->
						<!--					<div class="col-4">-->
						<!--						<div class="mt-4">-->
						<!--							<p class="mb-2 text-truncate"><i class="fas fa-circle text-success me-1"></i> Ali</p>-->
						<!--							<h5>$1,763</h5>-->
						<!--						</div>-->
						<!--					</div>-->
						<!--					<div class="col-4">-->
						<!--						<div class="mt-4">-->
						<!--							<p class="mb-2 text-truncate"><i class="fas fa-circle text-danger me-1"></i> Pending</p>-->
						<!--							<h5>$973</h5>-->
						<!--						</div>-->
						<!--					</div>-->
						<!--				</div>-->
						<!--			</div>-->
						<!--		</div>-->
						<!--	</div>-->
						<!--</div>-->
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="card">
								<div class="card-header">
									<div class="row">
										<div class="col">
											<h5 class="card-title">Recent Invoices</h5>
										</div>
										<div class="col-auto">
											<a href="invoices.html" class="btn-right btn btn-sm btn-outline-primary">
												View All 
											</a>
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="mb-3">
										<div class="progress progress-md rounded-pill mb-3">
											<div class="progress-bar bg-success" role="progressbar" style="width: 47%" aria-valuenow="47" aria-valuemin="0" aria-valuemax="100"></div>
											<div class="progress-bar bg-warning" role="progressbar" style="width: 28%" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
											<div class="progress-bar bg-danger" role="progressbar" style="width: 15%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100"></div>
											<div class="progress-bar bg-info" role="progressbar" style="width: 10%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
										<div class="row">
											<div class="col-auto">
												<i class="fas fa-circle text-success me-1"></i> Paid
											</div>
											<div class="col-auto">
												<i class="fas fa-circle text-warning me-1"></i> Unpaid
											</div>
											<div class="col-auto">
												<i class="fas fa-circle text-danger me-1"></i> Overdue
											</div>
											<div class="col-auto">
												<i class="fas fa-circle text-info me-1"></i> Draft
											</div>
										</div>
									</div>
				
									<div class="table-responsive table-index">
									
										<table class="table table-stripped table-hover " id="recent-invoice">
											<thead class="thead-light">
												<tr>
												   <th>Customer</th>
												   <th>Amount</th>
												   <th>Due Date</th>
												   <th>Status</th>
												   <th>Action</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<!--<div class="col-md-6 col-sm-6">-->
						<!--	<div class="card">-->
						<!--		<div class="card-header">-->
						<!--			<div class="row">-->
						<!--				<div class="col">-->
						<!--					<h5 class="card-title">Recent Estimates</h5>-->
						<!--				</div>-->
						<!--				<div class="col-auto">-->
						<!--					<a href="estimates.html" class="btn-right btn btn-sm btn-outline-primary">-->
						<!--						View All -->
						<!--					</a>-->
						<!--				</div>-->
						<!--			</div>-->
						<!--		</div>-->
						<!--		<div class="card-body">-->
						<!--			<div class="mb-3">-->
						<!--				<div class="progress progress-md rounded-pill mb-3">-->
						<!--					<div class="progress-bar bg-success" role="progressbar" style="width: 39%" aria-valuenow="39" aria-valuemin="0" aria-valuemax="100"></div>-->
						<!--					<div class="progress-bar bg-danger" role="progressbar" style="width: 35%" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>-->
						<!--					<div class="progress-bar bg-warning" role="progressbar" style="width: 26%" aria-valuenow="26" aria-valuemin="0" aria-valuemax="100"></div>-->
						<!--				</div>-->
						<!--				<div class="row">-->
						<!--					<div class="col-auto">-->
						<!--						<i class="fas fa-circle text-success me-1"></i> Sent-->
						<!--					</div>-->
						<!--					<div class="col-auto">-->
						<!--						<i class="fas fa-circle text-warning me-1"></i> Draft-->
						<!--					</div>-->
						<!--					<div class="col-auto">-->
						<!--						<i class="fas fa-circle text-danger me-1"></i> Expired-->
						<!--					</div>-->
						<!--				</div>-->
						<!--			</div>-->
						<!--			<div class="table-responsive table-index">-->
						<!--				<table class="table table-hover" id="recent-estimates">-->
						<!--					<thead class="thead-light">-->
						<!--						<tr>-->
						<!--							<th>Customer</th>-->
						<!--							<th>Expiry Date</th>-->
						<!--							<th>Amount</th>-->
						<!--							<th>Status</th>-->
						<!--							<th class="text-right">Action</th>-->
						<!--						</tr>-->
						<!--					</thead>-->
						<!--					<tbody>-->
												
						<!--					</tbody>-->
						<!--				</table>-->
						<!--			</div>-->
						<!--		</div>-->
						<!--	</div>-->
						<!--</div>-->
					</div>
				</div>
		
			<!-- /Page Wrapper -->

	@endsection
	

