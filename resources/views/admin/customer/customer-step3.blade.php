@extends('layout.mainlayout')
@section('content')
<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Customer Payment</h5>
								</div>
								<div class="card-body">
									<form   action="{{url('/customer/payment')}}"  method="post" id="paymentForm">
									    <input type="hidden" name="dataValue" id="dataValue" />
    <input type="hidden" name="dataDescriptor" id="dataDescriptor" />
									    <input type="hidden" value="{{$row->id??''}}" name="id">
									    @csrf
										
										
										 <button type="button"
        class="AcceptUI"
        data-billingAddressOptions='{"show":true, "required":false}' 
        data-apiLoginID="34J7CvcYJ" 
        data-clientKey="5aXszW969aWUbzHmTP57y86wbYBB2HDLmmNudT6jj8FvMHx47rurC7NvgM47WgZ8"
        data-acceptUIFormBtnTxt="Submit" 
        data-acceptUIFormHeaderTxt="Card Information" 
        data-responseHandler="responseHandler" style="padding: 10px 20px;
    font-size: 20px;
    float: right;margin: 10px;background-color: green;
    color: white;">Pay
    </button>
									</form>
								</div>
							</div>
						</div>
					</div>
					<script type="text/javascript">

function responseHandler(response) {
    if (response.messages.resultCode === "Error") {
        var i = 0;
        while (i < response.messages.message.length) {
            console.log(
                response.messages.message[i].code + ": " +
                response.messages.message[i].text
            );
            i = i + 1;
        }
    } else {
        paymentFormUpdate(response.opaqueData);
    }
}


function paymentFormUpdate(opaqueData) {
    document.getElementById("dataDescriptor").value = opaqueData.dataDescriptor;
    document.getElementById("dataValue").value = opaqueData.dataValue;

    // If using your own form to collect the sensitive data from the customer,
    // blank out the fields before submitting them to your server.
    // document.getElementById("cardNumber").value = "";
    // document.getElementById("expMonth").value = "";
    // document.getElementById("expYear").value = "";
    // document.getElementById("cardCode").value = "";

    document.getElementById("paymentForm").submit();
}
</script>
					@endsection