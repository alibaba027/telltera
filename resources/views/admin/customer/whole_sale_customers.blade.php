@extends('layout.mainlayout')
@section('css')
<style>
    .customer_list>td,.customer_list>th{
        text-align:center;
    }
</style>
@endsection
@section('content')	
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
			    @if(Auth::user()->role=='admin')
			    <h5 class="card-title">All Requests
				
				</h5>
			    @else
			    <h5 class="card-title">All Customers
				<a href="/customer/add" class="btn btn-primary btn-md" style="float:right"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add Customer
				</a>
				</h5>
			    @endif
				
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped mb-0">
						<thead>
							<tr class="customer_list">
								<th>ID</th>
								@if(Auth::user()->role=='admin')
								<th>Client</th>
								@else
								<th>Subscription ID</th>
								<th>Customer Name</th>
								@endif
								<th>Package Name</th>
								<th>Sim/Serial Number</th>
								<th>IMEI</th>
								<th>Price Total</th>
								
								<th>Status</th>
								<th>Date Created</th>
								@if(Auth::user()->role=='admin')
								<th>Action</th>
								@endif
							</tr>
						</thead>
						<tbody>
						    @foreach($rows as $key=>$row)
							<tr class="customer_list">
							   
							    <td >{{$key+1}}</td>
							    @if(Auth::user()->role=='admin')
							    <td>{{$row->client->first_name??''}}</td>
							    @else
							    <td>Sub-00{{$key+1}}</td>
								<td>{{ $row->customer_name}}</td>
								@endif
								<td>{{ $row->service_name??''}}</td>
								<td>{{ $row->sim}}</td>
								<td>{{ $row->imei}}</td>
								<td>${{ $row->total??0}}</td>
									
								<td > <span @if($row->status=='reject') class="badge bg-danger" @elseif($row->status=='paid') class="badge bg-success" @elseif($row->status=='Pending') class="badge bg-warning" @endif> {{ ucfirst($row->status)}}</span>  </td>
							<td>{{ $row->created_at??''}}</td>
							@if(Auth::user()->role=='admin')
								<td><a href="/activation/request/update/{{$row->id}}"><i class="fa fa-edit btn btn-primary"></i></a>
								<!--<a href="/customer/delete/{{$row->id}}"><i class="fa fa-trash btn btn-danger"></i></a>-->
								</td>
								@endif
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection