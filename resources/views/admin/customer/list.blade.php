@extends('layout.mainlayout')
@section('css')
<style>
    .customer_list>td,.customer_list>th{
        text-align:center;
    }
</style>
@endsection
@section('content')	
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title">All Customers
				<a href="/customer/add" class="btn btn-primary btn-md" style="float:right"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add Customer
				</a>
				</h5>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped mb-0">
						<thead>
							<tr class="customer_list">
								<th>ID</th>
								<th>Subscription ID</th>
								<th>Customer Name</th>
								<th>Package Name</th>
								<th>Sim/Serial Number</th>
								<th>IMEI</th>
								<th>Price Per Month</th>
								<th>Date Created</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						    @foreach($rows as $key=>$row)
							<tr class="customer_list">
							    <td >{{$key+1}}</td>
							    <td>Sub-00{{$key+1}}</td>
								<td>{{ $row->name}}</td>
								<td>{{ $row->service->name??''}}</td>
								<td>{{ $row->sim}}</td>
								<td>{{ $row->imei}}</td>
								<td>${{ $row->service->price_per_month??0}}</td>
									<td>{{ $row->created_at??''}}</td>
								<td > <span @if($row->status=='cancelled') class="badge bg-danger" @elseif($row->status=='active') class="badge bg-success" @elseif($row->status=='suspended') class="badge bg-warning" @endif> {{ $row->status}}</span>  </td>
							
								<td><a href="/customer/edit/{{$row->id}}"><i class="fa fa-edit btn btn-primary"></i></a>
								<a href="/customer/delete/{{$row->id}}"><i class="fa fa-trash btn btn-danger"></i></a>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection