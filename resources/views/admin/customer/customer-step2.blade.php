@extends('layout.mainlayout')
@section('content')
<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Customer Shipping Information</h5>
								</div>
								<div class="card-body">
									<form action="{{url('/customer/step2')}}" method="post">
									    <input type="hidden" value="{{$customer->id??''}}" name="id">
									    @csrf
										<div class="form-group row">
											<label class="col-form-label col-md-2">Customer Business Name</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="business_name" >
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Address</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="address" >
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">City</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="city" >
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">State</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="state" >
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Zip Code</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="zip" >
											</div>
										</div>
										
										<div class="form-group row">
											<label class="col-form-label col-md-2">Country</label>
											<div class="col-md-10">
													<input type="text" class="form-control" name="country" value="USA">
											</div>
											
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">Phone</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="phone" value="">
											</div>
											<div class="col-md-12">
												<hr>
											</div>
											<div class="col-md-12">
												<button type="submit" class="btn btn-primary btn-md" style="float:right">Submit</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					@endsection