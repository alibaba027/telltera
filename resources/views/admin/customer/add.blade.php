@extends('layout.mainlayout')
@section('css')
<style>
    .fe-check:before{
        
    background-color: #4A3AFF;
    color: white;
    border-radius: 50%;
    padding: 2px;

    }
</style>
@endsection
@section('content')
<div class="row">
				<div class="price-table-main">
						<div class="plan-selected">
							<h4>All Plans</h4>
						</div>
						<div class="row">
						    @foreach($services as $service)
						    @if($service->wholesale_client)
							    <div class="col-lg-4 col-md-12">
								<div class="card">
									<div class="card-body" style="min-height: 320px;">
										<div class="plan-header">
										
											<div class="plan-title">
												<h4 class="plan-name">{{$service->name}}</h4>
											</div>
										</div>
										<div class="description-content">								
											<!--<p>Lorem ipsum dolor sit amet doloroli sitiol conse ctetur adipiscing elit. </p>-->
										</div>
										<div class="price-dollar">
											<p class="align-items-center" style="text-align: center;
    font-size: 18px;">${{$service->price_per_month}}<span class="ms-1">/monthly</span></p>
										</div>
										<div class="plan-description" style="margin-top:20px">
											<h6>What’s included</h6>
											<ul style="padding-left:5px">
											    <li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
													{{$service->category->name}}
												</li>
												<li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
												    Activation Fee ${{$service->activation_fee}}
												</li>
												<li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
													Per Month Fee ${{$service->price_per_month}}
												</li>
												
												
											</ul>
										</div>
										<div class="plan-button" style="    bottom: 25px;margin-top: 20px;position: absolute;width: 85%;"> 
											<a class="btn btn-primary d-flex align-items-center justify-content-center" href="/service/purchase/{{$service->id}}">Get Started<span class="ms-2"><i class="fe fe-arrow-right"></i></span></a>
										</div>
									</div>
								</div>
							</div>
							@endif
							@endforeach
						</div>
					</div>
						    
						    
						    
							<!--<div class="card">-->
							<!--	<div class="card-header">-->
							<!--		<h5 class="card-title">Customer</h5>-->
							<!--	</div>-->
							<!--	<div class="card-body">-->
								    
								    
									<!--<form @if(isset($row)) action="{{url('/customer/update')}}" @else action="{{url('/customer/store')}}" @endif method="post">-->
									<!--    <input type="hidden" value="{{$row->id??''}}" name="id">-->
									<!--    @csrf-->
									<!--	<div class="form-group row">-->
									<!--		<label class="col-form-label col-md-2">Customer Name</label>-->
									<!--		<div class="col-md-10">-->
									<!--			<input type="text" class="form-control" name="name" value="{{$row->name??''}}">-->
									<!--		</div>-->
									<!--	</div>-->
									<!--	<div class="form-group row">-->
									<!--		<label class="col-form-label col-md-2">Customer Email</label>-->
									<!--		<div class="col-md-10">-->
									<!--			<input type="text" class="form-control" name="email" value="{{$row->email??''}}">-->
									<!--		</div>-->
									<!--	</div>-->
										<!--<div class="form-group row">-->
										<!--	<label class="col-form-label col-md-2">Sim/Serial Number</label>-->
										<!--	<div class="col-md-10">-->
										<!--		<input type="text" class="form-control" name="sim" value="{{$row->sim??''}}">-->
										<!--	</div>-->
										<!--</div>-->
									<!--	<div class="form-group row">-->
									<!--		<label class="col-form-label col-md-2">IMEI</label>-->
									<!--		<div class="col-md-10">-->
									<!--			<input type="text" class="form-control" name="imei" value="{{$row->imei??''}}">-->
									<!--		</div>-->
									<!--	</div>-->
										
									<!--	<div class="form-group row">-->
									<!--		<label class="col-form-label col-md-2">Service</label>-->
									<!--		<div class="col-md-10">-->
									<!--			<select class="form-select" name="service_id">-->
									<!--				<option value="">-- Select --</option>-->
									<!--				@foreach($services as $service)-->
									<!--				<option @if(isset($row) && $row->service_id==$service->id) selected @endif value="{{$service->id}}">{{$service->name}} (${{$service->price_per_month}})</option>-->
									<!--				@endforeach-->
									<!--			</select>-->
									<!--		</div>-->
											
									<!--	</div>-->
									<!--	<div class="form-group row">-->
									<!--		<label class="col-form-label col-md-2">Status Select</label>-->
									<!--		<div class="col-md-10">-->
									<!--			<select class="form-select" name="status">-->
									<!--				<option value="">-- Select --</option>-->
												
												
									<!--				<option @if(isset($row) && $row->status=='active') selected @endif value="active">Active</option>-->
									<!--					<option @if(isset($row) && $row->status=='suspended') selected @endif value="suspended">Suspended</option>-->
									<!--					<option @if(isset($row) && $row->status=='cancelled') selected @endif value="cancelled">Cancelled</option>-->
									<!--			</select>-->
									<!--		</div>-->
									<!--		<div class="col-md-12">-->
									<!--			<hr>-->
									<!--		</div>-->
									<!--		<div class="col-md-12">-->
									<!--			<button type="submit" class="btn btn-primary btn-md" style="float:right">Submit</button>-->
									<!--		</div>-->
									<!--	</div>-->
									<!--</form>-->
						<!--		</div>-->
						<!--	</div>-->
						<!--</div>-->
					</div>
					@endsection