@extends('layout.mainlayout')
@section('content')
<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title"> Ticket History</h5>
								</div>
								<div class="card-body">
								    <div class="row">
								        <div class="col-lg-6">
								            <b>Title: </b>{{$ticket->title}}
								            <br>
								            <b>Body: </b>{{$ticket->body}}
								        </div>
								        <div class="col-lg-6">
								            <b>Attachment: </b>
								            <br>
								            <a href="/images/{{$ticket->image}}" target="_blank"><img src="/images/{{$ticket->image}}" style="width:300px;margin-bottom:30px"></a>
								        </div>
								    </div>
								  
								</div>
							</div>
						</div>
						@if(count($ticket->repries))
						@foreach($ticket->repries as $key=>$reply)
						<div class="card">
							<div class="card-body">
								<div class="comments-details d-flex align-items-center justify-content-between">
									<div class="d-flex align-items-center">
										<span class="comments-widget-img rounded-circle d-inline-flex">
											<img class="avatar-img rounded-circle" 
											@if($reply->reply_name=='System Admin')
											src="/images/admin.jpg"
											@else
											src="/images/user1.png"
											@endif
											alt="User Image" style="width: 80px;padding: 10px;">
										</span>
										<div class="comments-details-cont">
											<h6>{{$reply->reply_name}}</h6>
											<p >{{$reply->created_at}}</p>
										</div>
									</div>
								</div>
								<div class="card-describe">
									<p style="padding-left:20px"><b>Reply: </b>{{$reply->reply??''}}</p>
									@if($reply->image)
									<p style="padding-left:20px"><b>Attachment: </b> <br><a href="/images/{{$reply->image}}" target="_blank"><img src="/images/{{$reply->image}}" style="width:300px;padding-top: 10px;"></a></p>
									@endif
								</div>
							</div>
						</div>
						@endforeach
						@endif
						@if($ticket->status!='Closed')
						<div class="card">
							<div class="card-body">
								<div class="comments-details d-flex align-items-center justify-content-between">
									<div class="d-flex align-items-center" style="width:100%">
											<form action="{{url('/ticket/reply')}}" method="post" enctype='multipart/form-data' style="width:100%">
									   <h5>Add Reply</h5>
									    @csrf
										<div class="form-group row">
										   <div class="col-lg-6 col-md-12">
        										<label class="col-form-label col-md-12">Description</label>
        											<div class="col-md-12">
        												<textarea type="text" class="form-control" name="deacription" ></textarea>
        											</div>
										    </div>
										    <input type="hidden" value="{{$ticket->id}}" name="id">
										    <div class="col-lg-6 col-md-12">
        										<label class="col-form-label col-md-2">Image</label>
        											<div class="col-md-12">
        												<input type="file" class="form-control" name="image" >
        											</div>
										    </div>
										    @if(Auth::user()->role=='admin')
											<div class="col-lg-6 col-md-12">
        										<label class="col-form-label col-md-2">Status</label>
        											<div class="col-md-12">
        												<select class="form-control" name="status">
        												    <option value="in progress">In progress</option>
        												    <option value="Closed">Closed</option>
        												        
        												</select>
        											</div>
										    </div>
										    @endif
										    
										</div>
										
										<input type="hidden" name="type" value="Service">
    										
									
										
										<div class="form-group row">
										
											<div class="col-md-12">
												<button type="submit" class="btn btn-primary btn-sm">Submit</button>
											</div>
										</div>
									</form>
									</div>
								</div>
								
							</div>
						</div>
						@endif
					</div>
					@endsection
					