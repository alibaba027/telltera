@extends('layout.mainlayout')
@section('content')	
	<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">@if(isset($type) && $type=='service') Services @else Ticket @endif Categories
									<a @if(isset($type) && $type=='service') href="/service/category/add" @else href="/category/add" @endif class="btn btn-primary btn-sm" style="float:right"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add</a></h5>
									
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-striped mb-0">
											<thead>
												<tr>
													<th>Name</th>
													@if(isset($type) && $type=='service')<th>Parent</th>@endif
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											    @foreach($rows as $row)
												<tr>
													<td>{{ $row->name}}</td>
													@if(isset($type) && $type=='service')<td>{{ $row->parent->name??''}}</td>@endif
													<td > <span @if($row->status=='draft') class="badge bg-secondary" @elseif($row->status=='publish') class="badge bg-success" @elseif($row->status=='pending') class="badge bg-warning" @endif> {{ $row->status}}</span>  </td>
													<td><a href="/category/edit/{{$row->id}}"><i class="fa fa-edit btn btn-primary"></i></a>
													<a href="/category/delete/{{$row->id}}"><i class="fa fa-trash btn btn-danger"></i></a>
													</td>
												</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						@endsection