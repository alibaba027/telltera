@extends('layout.mainlayout')
@section('content')
<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title"> Category for @if(isset($type) && $type=='service') Services @else Tickets @endif</h5>
								</div>
								<div class="card-body">
									<form @if(isset($row)) action="{{url('/category/update')}}" @else action="{{url('/category/store')}}" @endif method="post">
									    <input type="hidden" value="{{$row->id??''}}" name="id">
									    @csrf
										<div class="form-group row">
											<label class="col-form-label col-md-2">Category Name</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="name" value="{{$row->name??''}}">
											</div>
										</div>
										@if((isset($type) && $type=='service') || (isset($row) && $row->type=='Service'))
										<input type="hidden" name="type" value="Service">
    										<div class="form-group row">
    											<label class="col-form-label col-md-2">Parent Select</label>
    											<div class="col-md-10">
    												<select class="form-select" name="parent_id">
    													<option value="">-- Select --</option>
    													@foreach($categories as $cate)
    													<option @if(isset($row) && $row->parent_id==$cate->id) selected @endif value="{{$cate->id}}">{{$cate->name}}</option>
    													@endforeach
    												</select>
    											</div>
    											
    										</div>
										@endif
										
										<div class="form-group row">
											<label class="col-form-label col-md-2">Status Select</label>
											<div class="col-md-10">
												<select class="form-select" name="status">
													<option>-- Select --</option>
													<option @if(isset($row) && $row->status=='draft') selected @endif value="draft">Draft</option>
													<option @if(isset($row) && $row->status=='pending') selected @endif value="pending">Pending for review</option>
													<option @if(isset($row) && $row->status=='publish') selected @endif value="publish">Publish</option>
												</select>
											</div>
											<div class="col-md-12">
												<button type="submit" class="btn btn-primary btn-sm">Submit</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					@endsection