@extends('layout.mainlayout')
@section('content')
<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title"> Add Ticket</h5>
								</div>
								<div class="card-body">
									<form action="{{url('/ticket/store')}}" method="post" enctype='multipart/form-data'>
									   
									    @csrf
										<div class="form-group row">
										    <div class="col-lg-6 col-md-12">
        										<label class="col-form-label col-md-2">Title</label>
        											<div class="col-md-12">
        												<input type="text" class="form-control" name="title" >
        											</div>
										    </div>
										    <div class="col-lg-6 col-md-12">
        										<label class="col-form-label col-md-12">Category </label>
    											<div class="col-md-12">
    												<select class="form-select" name="category_id">
    													<option value="">-- Select --</option>
    													@foreach($categories as $cate)
    													<option @if(isset($row) && $row->parent_id==$cate->id) selected @endif value="{{$cate->id}}">{{$cate->name}}</option>
    													@endforeach
    												</select>
    											</div>
										    </div>
										    <div class="col-lg-6 col-md-12">
        										<label class="col-form-label col-md-2">Image</label>
        											<div class="col-md-12">
        												<input type="file" class="form-control" name="image" >
        											</div>
										    </div>
											<div class="col-lg-6 col-md-12">
        										<label class="col-form-label col-md-12">Description</label>
        											<div class="col-md-12">
        												<textarea type="text" class="form-control" name="deacription" ></textarea>
        											</div>
										    </div>
										    
										</div>
										
										<input type="hidden" name="type" value="Service">
    										
									
										
										<div class="form-group row">
										
											<div class="col-md-12">
												<button type="submit" class="btn btn-primary btn-sm">Submit</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					@endsection