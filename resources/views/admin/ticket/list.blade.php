@extends('layout.mainlayout')
@section('content')	
	<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">All Tickets
									<a  href="/ticket/add" class="btn btn-primary btn-sm" style="float:right"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add</a>
									</h5>
									
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-striped mb-0">
											<thead>
												<tr>
													<th>Title</th>
													<th>Body</th>
													<th>Attachment</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											    @foreach($tickets as $row)
												<tr>
													<td>{{ $row->title}}</td>
													<td>{{ $row->body}}</td>
													<td>
													    @if($row->image)<a href="/images/{{ $row->image}}" target="_blank"><img src="/images/{{ $row->image}}" style="width:100px"></a>
													    @endif</td>
													<td > <span @if($row->status=='in progress') class="badge bg-warning" @elseif($row->status=='Closed') class="badge bg-success" @elseif($row->status=='pending') class="badge bg-warning" @endif> {{ $row->status}}</span>  </td>
													<td>
													    <a href="/ticket/view/{{$row->id}}">View</a>
													</td>
												</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						@endsection