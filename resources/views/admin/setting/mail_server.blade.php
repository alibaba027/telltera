<?php $page="settings";?>
@extends('layout.mainlayout')
@section('content')		
@component('components.breadcrumb')                
    @slot('title') Settings @endslot
    @slot('li_1') Dashboard @endslot
    @slot('li_3') Profile Settings @endslot
@endcomponent

	<div class="row">
		<div class="col-xl-3 col-md-4">
		
			<!-- Settings Menu -->
			<div class="widget settings-menu">
				<ul>
					<li class="nav-item">
						<a href="{{url('settings')}}" class="nav-link">
							<i class="far fa-user"></i> <span>Profile Settings</span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('/setting/mail_server')}}" class="nav-link active">
							<i class="fas fa-envelope"></i> <span>Email Server</span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('preferences')}}" class="nav-link">
							<i class="fas fa-cog"></i> <span>Preferences</span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('tax-types')}}" class="nav-link">
							<i class="far fa-check-square"></i> <span>Tax Types</span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('expense-category')}}" class="nav-link">
							<i class="far fa-list-alt"></i> <span>Expense Category</span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('notifications')}}" class="nav-link">
							<i class="far fa-bell"></i> <span>Notifications</span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('change-password')}}" class="nav-link">
							<i class="fas fa-unlock-alt"></i> <span>Change Password</span>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('delete-account')}}" class="nav-link">
							<i class="fas fa-ban"></i> <span>Delete Account</span>
						</a>
					</li>
				</ul>
			</div>
			<!-- /Settings Menu -->
			
		</div>
		
		<div class="col-xl-9 col-md-8">
		
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">Mail Server</h5>
				</div>
				<div class="card-body">
				
					<!-- Form -->
					<form action="{{url('/setting/store/mail_server')}}" method="post">
					    @csrf
						<div class="row form-group">
							<label for="name" class="col-sm-3 col-form-label input-label">Mail Driver</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="mail_driver" placeholder="" value="{{$setting->where('key','mail_driver')[0]->value??''}}">
							</div>
						</div>
						<div class="row form-group">
							<label for="name" class="col-sm-3 col-form-label input-label">Mail Host</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="mail_host" placeholder="" value="{{$setting->where('key','mail_host')[1]->value??''}}">
							</div>
						</div>
						<div class="row form-group">
							<label for="name" class="col-sm-3 col-form-label input-label">Mail Port</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="mail_port" placeholder="" value="{{$setting->where('key','mail_port')[2]->value??''}}">
							</div>
						</div>
						<div class="row form-group">
							<label for="name" class="col-sm-3 col-form-label input-label">Mail Username</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="mail_username" placeholder="" value="{{$setting->where('key','mail_username')[3]->value??''}}">
							</div>
						</div>
						<div class="row form-group">
							<label for="name" class="col-sm-3 col-form-label input-label">Mail Password</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="mail_password" placeholder="" value="{{$setting->where('key','mail_password')[4]->value??''}}">
							</div>
						</div>
						<div class="row form-group">
							<label for="name" class="col-sm-3 col-form-label input-label">Mail Encryption</label>
							<div class="col-sm-9">
								<input type="text" class="form-control" name="mail_encryption" placeholder="" value="{{$setting->where('key','mail_encryption')[5]->value??''}}">
							</div>
						</div>
						<div class="text-end">
							<button type="submit" class="btn btn-primary">Save Changes</button>
						</div>
					</form>
					<!-- /Form -->
					
				</div>
			</div>
		</div>
	</div>
		
@endsection