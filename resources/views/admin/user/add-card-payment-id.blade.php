@extends('layout.mainlayout')
@section('content')
<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Add User PaymentID </h5>
								</div>
								<div class="card-body">
									<form  action="{{url('/user/store/card-payment-id')}}" method="post">
									    <input type="hidden" value="{{$id??''}}" name="id">
									    @csrf
										
										<div class="form-group row">
											<label class="col-form-label col-md-2">Payment ID</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="payment_id" value="">
											</div>
										</div>
										
											<div class="col-md-12">
												<hr>
											</div>
											<div class="col-md-12" style="padding-bottom:35px">
												<button type="submit" class="btn btn-primary btn-md" style="float:right">Submit</button>
											</div>
										
									</form>
								</div>
							</div>
						</div>
					</div>
					@endsection