@extends('layout.mainlayout')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title">Profile</h5>
			</div>
			<div class="card-body">
				<form action="{{url('/user/profile/edit/update')}}" method="post">
				    <input type="hidden" value="{{Auth::user()->id??''}}" name="id">
				    @csrf
					<div class="form-group row">
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-2">Fist Name</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="first_name" value="{{Auth::user()->first_name??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-2">Last Name</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="last_name" value="{{Auth::user()->last_name??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-3">Business Name</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="business_name" value="{{Auth::user()->business_name??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-2">Email</label>
    						<div class="col-md-10">
    							<input type="email" class="form-control" name="email" value="{{Auth::user()->email??''}}" disabled>
    						</div>
					    </div>
					    
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">New Password</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="password" value="">
    						</div>
					    </div>
					    
						
					</div>
					<div class="col-md-12">
						<hr>
					</div>
					<div class="col-md-12" style="padding-bottom:35px">
						<button type="submit" class="btn btn-primary btn-md" style="float:right">Submit</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
@endsection