@extends('layout.mainlayout')
@section('css')
<style>
    .customer_list>td,.customer_list>th{
        text-align:center;
    }
</style>
@endsection
@section('content')	

	<div class="col-lg-12">
	    <div class="card">
			<div class="card-header">
				<h5 class="card-title">All {{ucfirst(str_replace('_',' ',$type))}}s
				<a href="/user/{{$type}}/add" class="btn btn-primary btn-md" style="float:right"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add {{ucfirst(str_replace('_',' ',$type))}}</a>
				</h5>
				
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped mb-0">
						<thead>
							<tr class="customer_list">
								<th>#</th>
								<th> First Name</th>
								<th>Last Name</th>
								<th>Payment Method</th>
								<th>Email</th>
								<th>Role</th>
								@if($type=='wholesale_client')
								<th>Authorize ID</th>
								<th>Payment ID</th>
								@endif
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						    @foreach($rows as $key=>$row)
							<tr class="customer_list">
							    <td >{{$key+1}}</td>
								<td>{{ $row->first_name}}</td>
								<td>{{ $row->last_name}}</td>
								<td>
								   <span  @if($row->payment_method=='online') class="badge badge-pill bg-success-light" @elseif($row->payment_method=='offline') class="badge badge-pill bg-danger-light" @endif> {{ $row->payment_method}}</span> 
								    </td>
								<td>{{ $row->email}}</td>
								<td>{{ ucfirst(str_replace('_',' ',$row->role))}}</td>
								@if($type=='wholesale_client')
								<td>{{ $row->profileid}}</td>
								<td>@if(!empty($row->paymentprofileid))********@endif</td>
								@endif
							
								<td > <span @if($row->status=='cancelled') class="badge bg-danger" @elseif($row->status=='active') class="badge bg-success" @elseif($row->status=='suspended') class="badge bg-warning" @endif> {{ $row->status}}</span>  </td>
							
								<td><a href="/user/{{$type}}/edit/{{$row->id}}"><i class="fa fa-edit btn btn-primary"></i></a>
								<a href="/user/{{$type}}/delete/{{$row->id}}"><i class="fa fa-trash btn btn-danger"></i></a>
								
								@if($type=='wholesale_client' && !$row->profileid)
									<a href="/user/{{$type}}/card/{{$row->id}}"><i class="fa fa-credit-card btn btn-warning"></i></a>
								@endif
								@if($type=='wholesale_client' && $row->profileid && !$row->paymentprofileid)
								<a href="/user/{{$type}}/card/payment-id/{{$row->id}}"><i class="fa fa-window-restore btn btn-warning"></i></a>
								@endif
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection