@extends('layout.mainlayout')
@section('content')
<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Create User Profile </h5>
								</div>
								<div class="card-body">
									<form  action="{{url('/user/store/card')}}" method="post">
									    <input type="hidden" value="{{$id??''}}" name="id">
									    @csrf
										<!--<div class="form-group row">-->
										<!--	<label class="col-form-label col-md-2">Card number</label>-->
										<!--	<div class="col-md-10">-->
										<!--		<input type="text" class="form-control" name="card_number" value="4242424242424242">-->
										<!--	</div>-->
										<!--</div>-->
										<!--<div class="form-group row">-->
										<!--	<label class="col-form-label col-md-2">Expiration Month</label>-->
										<!--	<div class="col-md-10">-->
										<!--		<input type="text" class="form-control" name="month" value="12">-->
										<!--	</div>-->
										<!--</div>-->
										<!--<div class="form-group row">-->
										<!--	<label class="col-form-label col-md-2">Expiration Year</label>-->
										<!--	<div class="col-md-10">-->
										<!--		<input type="text" class="form-control" name="year" value="2038">-->
										<!--	</div>-->
										<!--</div>-->
										<!--<div class="form-group row">-->
										<!--	<label class="col-form-label col-md-2">CVC</label>-->
										<!--	<div class="col-md-10">-->
										<!--		<input type="text" class="form-control" name="cvc" value="142">-->
										<!--	</div>-->
										<!--</div>-->
										<div class="form-group row">
											<label class="col-form-label col-md-2">Address</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="address" value="14 Main Street">
											</div>
										</div>
										<div class="form-group row">
											<label class="col-form-label col-md-2">City</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="city" value="Pecan Springs">
											</div>
											</div>
											
											<div class="form-group row">
											<label class="col-form-label col-md-2">State</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="state" value="TX">
											</div>
											</div>
											<div class="form-group row">
											<label class="col-form-label col-md-2">Country</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="country" value="USA">
											</div>
											</div>
											<div class="form-group row">
											<label class="col-form-label col-md-2">Phone</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="phone" value="888-888-8888">
											</div>
											</div>
											<div class="form-group row">
											<label class="col-form-label col-md-2">Zip</label>
											<div class="col-md-10">
												<input type="text" class="form-control" name="zip" value="44628">
											</div>
											</div>
											<div class="col-md-12">
												<hr>
											</div>
											<div class="col-md-12" style="padding-bottom:35px">
												<button type="submit" class="btn btn-primary btn-md" style="float:right">Submit</button>
											</div>
										
									</form>
								</div>
							</div>
						</div>
					</div>
					@endsection