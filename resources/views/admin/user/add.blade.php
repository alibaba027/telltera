@extends('layout.mainlayout')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title">{{ucfirst($type)}}</h5>
			</div>
			<div class="card-body">
				<form @if(isset($row)) action="{{url('/user/'.$type.'/update')}}" @else action="{{url('/user/'.$type.'/store')}}" @endif method="post">
				    <input type="hidden" value="{{$row->id??''}}" name="id">
				    <input type="hidden" value="{{$type}}" name="role">
				    @csrf
					<div class="form-group row">
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-2">Fist Name</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="first_name" value="{{$row->first_name??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-2">Last Name</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="last_name" value="{{$row->last_name??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-3">Business Name</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="business_name" value="{{$row->business_name??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-2">Email</label>
    						<div class="col-md-10">
    							<input type="email" class="form-control" name="email" value="{{$row->email??''}}">
    						</div>
					    </div>
					    @if($type=='dealer')
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-5">Commission Percentage</label>
    						<div class="col-md-10">
    							<input type="number" class="form-control" name="commission_percentage" value="{{$row->commission_percentage??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-5">Fixed Percentage</label>
    						<div class="col-md-10">
    							<input type="number" class="form-control" name="fixed_percentage" value="{{$row->fixed_percentage??''}}">
    						</div>
					    </div>
					    @endif
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-2">Password</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="password" value="">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-3">Status Select</label>
    						<div class="col-md-10">
    							<select class="form-select" name="status">
    								<option value="">-- Select --</option>
    							
    							
    								<option @if(isset($row) && $row->status=='active') selected @endif value="active">Active</option>
    									<option @if(isset($row) && $row->status=='suspended') selected @endif value="suspended">Suspended</option>
    									<option @if(isset($row) && $row->status=='cancelled') selected @endif value="cancelled">Cancelled</option>
    							</select>
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-4">Payment Method</label>
    						<div class="col-md-10">
    							<select class="form-select" name="payment_method">
    								<option value="">-- Select --</option>
    							
    							
    								<option @if(isset($row) && $row->payment_method=='online') selected @endif value="online">Online</option>
    									<option @if(isset($row) && $row->status=='offline') selected @endif value="offline">Offline</option>
    								
    							</select>
    						</div>
					    </div>
						
					</div>
					<div class="col-md-12">
						<hr>
					</div>
					<div class="col-md-12" style="padding-bottom:35px">
						<button type="submit" class="btn btn-primary btn-md" style="float:right">Submit</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
@endsection