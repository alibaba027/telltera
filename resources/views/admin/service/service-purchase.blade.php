@extends('layout.mainlayout')
@section('content')
<div class="row">
    <div class="col-lg-6 col-md-12">
        							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Service Detail</h5>
								</div>
								<div class="card-body">
								    
										<div class="form-group row">
											
											<div class="col-md-12">
											<h5 style="text-align:center">{{$service->name}}</h5>	
											</div>
										</div>
										<div class="form-group row">
											
											<div class="col-md-12" style="text-align:center">
											<b>	${{$service->price_per_month}}/Monthly</b>
											</div>
										</div>
										<div class="form-group row">
											
											<div class="col-md-12">
											<div class="plan-description">
											<h6>What’s included</h6>
											<ul style="padding-left:5px">
											    <li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
													{{$service->category->name}}
												</li>
												<li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
												    Activation Fee ${{$service->activation_fee}}
												</li>
												<li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
													Per Month Fee ${{$service->price_per_month}}
												</li>
												
												
											</ul>
										</div>
											</div>
										</div>
										
									</div>
							</div>
    </div>
    <div class="col-lg-6 col-md-12">
        							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Customer Detail</h5>
								</div>
								<div class="card-body">
								    
								    
									<form  action="{{url('/service/purchase')}}" method="post">
									    <input type="hidden" value="{{$service->id??''}}" name="id">
									    @csrf
										<div class="form-group row">
											<label class="col-form-label col-md-12">Customer Name</label>
											<div class="col-md-12">
												<input type="text" class="form-control" name="name" value="{{$row->name??''}}">
											</div>
										</div>
										@if($service->sim)
										<div class="form-group row">
											<label class="col-form-label col-md-12">Sim/Serial Number</label>
											<div class="col-md-12">
												<input type="text" class="form-control" name="sim" value="{{$row->sim??''}}">
											</div>
										</div>
										@endif
										@if($service->imei)
										<div class="form-group row">
											<label class="col-form-label col-md-12">IMEI</label>
											<div class="col-md-12">
												<input type="text" class="form-control" name="imei" value="{{$row->imei??''}}">
											</div>
										</div>
										@endif
										<div class="form-group row">
											
											<div class="col-md-12">
												<button class="btn btn-primary" type="submit" style="width:100%">Purchase</button>
											</div>
										</div>
										</form>
									</div>
							</div>
    </div>
    
</div>


@endsection
