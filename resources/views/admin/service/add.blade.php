@extends('layout.mainlayout')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h5 class="card-title">Service</h5>
			</div>
			<div class="card-body">
				<form @if(isset($row)) action="{{url('/service/update')}}" @else action="{{url('/service/store')}}" @endif method="post">
				    <input type="hidden" value="{{$row->id??''}}" name="id">
				    @csrf
					<div class="form-group row">
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-4">Service Name</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="name" value="{{$row->name??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-2">Category</label>
    						<div class="col-md-10">
    							<select class="form-select" name="category_id">
    								<option value="">-- Select --</option>
    								@foreach($categories as $cate)
    								<option @if(isset($row) && $row->category_id==$cate->id) selected @endif value="{{$cate->id}}">{{$cate->name}}</option>
    								@endforeach
    							</select>
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Price Per Month</label>
    						<div class="col-md-10">
    							<input type="number" class="form-control" name="price_per_month" value="{{$row->price_per_month??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Status Select</label>
						<div class="col-md-10">
							<select class="form-select" name="status">
								<option value="">-- Select --</option>
								<option @if(isset($row) && $row->status=='draft') selected @endif value="draft">Draft</option>
								<option @if(isset($row) && $row->status=='pending') selected @endif value="pending">Pending for review</option>
								<option @if(isset($row) && $row->status=='publish') selected @endif value="publish">Publish</option>
							</select>
						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Equipment Name</label>
    						<div class="col-md-10">
    							<input type="text" class="form-control" name="equipment_name" value="{{$row->equipment_name??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Equipment Price</label>
    						<div class="col-md-10">
    							<input type="number" class="form-control" name="equipment_price" value="{{$row->equipment_price??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Activation Fee</label>
    						<div class="col-md-10">
    							<input type="number" class="form-control" name="activation_fee" value="{{$row->activation_fee??''}}">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Renew Eveny Month At</label>
    						<div class="col-md-10">
    							<input type="number" class="form-control" name="renew_every_month" value="{{$row->renew_every_month??''}}" max="31">
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Show To Client</label>
    						<div class="col-md-10">
    							<input type="checkbox"  name="client" value="1" @if($row->client) checked @endif>
    						</div>
					    </div>
					     <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Show To Wholesales Client</label>
    						<div class="col-md-10">
    							<input type="checkbox"  name="wholesale_client" value="1" @if($row->wholesale_client) checked @endif>
    						</div>
					    </div>
					     <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Show To Dealer</label>
    						<div class="col-md-10">
    							<input type="checkbox"  name="dealer" value="1" @if($row->dealer) checked @endif>
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Required Sim Number</label>
    						<div class="col-md-10">
    							<input type="checkbox"  name="sim" value="1" @if($row->sim) checked @endif>
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					        <label class="col-form-label col-md-12">Required IMEI</label>
    						<div class="col-md-10">
    							<input type="checkbox"  name="imei" value="1" @if($row->imei) checked @endif>
    						</div>
					    </div>
					    <div class="col-lg-6 col-md-12">
					    </div>
						
					</div>
					
					<div class="form-group row">
						
						<div class="col-md-12">
							<hr>
						</div>
						<div class="col-md-12">
							<button type="submit" class="btn btn-primary btn-md" style="float:right">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection