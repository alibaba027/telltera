@extends('layout.mainlayout')
@section('content')
<div class="row">
    <div class="col-lg-6 col-md-12">
        							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Request Detail</h5>
								</div>
								<div class="card-body">
								    
										<div class="form-group row">
											
											<div class="col-md-12">
											<h5 style="text-align:center">{{$service->name}}</h5>	
											</div>
										</div>
										<div class="form-group row">
											
											<div class="col-md-12" style="text-align:center">
											<b>	${{$service->price_per_month}}/Monthly</b>
											</div>
										</div>
										<div class="form-group row">
											
											<div class="col-md-12">
											<div class="plan-description">
											<h6>What’s included</h6>
											<ul style="padding-left:5px">
											    <li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
													{{$service->category->name}}
												</li>
												<li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
												    Activation Fee ${{$service->activation_fee}}
												</li>
												<li>
													<span class="rounded-circle me-2"><i class="fe fe-check"></i></span>
													Per Month Fee ${{$service->price_per_month}}
												</li>
												
												
											</ul>
										</div>
											</div>
										</div>
										
									</div>
							</div>
    </div>
    <div class="col-lg-6 col-md-12">
        							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Customer Detail</h5>
								</div>
								<div class="card-body">
								    
								    
									    <input type="hidden" value="{{$service->id??''}}" name="id">
									    @csrf
										<div class="form-group row">
											<label class="col-form-label col-md-12">Customer Name</label>
											<div class="col-md-12">
											{{$row->service_name??''}}
											</div>
										</div>
										@if($service->sim)
										<div class="form-group row">
											<label class="col-form-label col-md-12">Sim/Serial Number</label>
											<div class="col-md-12">
											{{$row->sim??''}}
											</div>
										</div>
										@endif
										@if($service->imei)
										<div class="form-group row">
											<label class="col-form-label col-md-12">IMEI</label>
											<div class="col-md-12">
												{{$row->imei??''}}
											</div>
										</div>
										@endif
										<div class="form-group row">
											
											<div class="col-md-12">
												<a href="/activation/request/{{$row->id}}/paid" class="btn btn-success">Paid</a>
												<a href="/activation/request/{{$row->id}}/reject" class="btn btn-danger">Reject</a>
											</div>
										</div>
									
									</div>
							</div>
    </div>
    
</div>


@endsection
