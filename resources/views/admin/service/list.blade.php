@extends('layout.mainlayout')
@section('content')	
	<div class="col-lg-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">Services</h5>
									
									<a href="/service/add" class="btn btn-primary btn-md" style="float:right;margin-left: 10px;"><i class="fa fa-plus"></i> &nbsp;&nbsp;Add</a>
									<form method="get" action="/service/list">
									<button type="submit" class="btn btn-success btn-md" style="float: right;">Filter</button>&nbsp;&nbsp;
									<select class="form-control" style="width: 20%;float: right; height: 38px;
    margin-right: 5px;" name="cate">
									    <option>select category</option>
									    @foreach($categories as $cate)
									        <option @if(isset($filter['cate']) && $filter['cate']==$cate->id) selected @endif value="{{$cate->id}}">{{$cate->name}}</option>
									    @endforeach
									</select>
									</form>
								</div>
								<div class="card-body">
									<div class="table-responsive">
										<table class="table table-striped mb-0">
											<thead>
												<tr>
													<th>Name</th>
													<th>Category</th>
													<th>Registration Fee</th>
													<th>Price Per Month</th>
													<th>Renew Every Month</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
											    @foreach($rows as $row)
												<tr>
													<td>{{ $row->name}}</td>
													<td>{{ $row->category->name??''}}</td>
													<td>${{ $row->activation_fee}}</td>
													<td>${{ $row->price_per_month}}</td>
														<td>{{ $row->renew_every_month}}</td>	
													<td > <span @if($row->status=='draft') class="badge bg-secondary" @elseif($row->status=='publish') class="badge bg-success" @elseif($row->status=='pending') class="badge bg-warning" @endif> {{ $row->status}}</span>  </td>
												
													<td><a href="/service/edit/{{$row->id}}"><i class="fa fa-edit btn btn-primary"></i></a>
													<a href="/service/delete/{{$row->id}}"><i class="fa fa-trash btn btn-danger"></i></a>
													</td>
												</tr>
											@endforeach
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						@endsection