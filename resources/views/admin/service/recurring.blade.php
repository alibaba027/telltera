@extends('layout.mainlayout')
@section('css')
<style>
    .customer_list>td,.customer_list>th{
        text-align:center;
    }
</style>
@endsection
@section('content')	
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
			    @if(Auth::user()->role=='admin')
			    <h5 class="card-title">All Requests
				
				</h5>
			    @else
			    <h5 class="card-title">Recurring
				<a class="btn btn-success" style="float: right;">Pay All ${{$total}}</a>&nbsp;&nbsp;
				<a href="/invoice/all/serices/download" class="btn btn-success" style="float: right;margin-right:10px"><i class="fa fa-download"></i> Download Invoices</a>&nbsp;&nbsp;
				</h5>
			    @endif
				
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-striped mb-0">
						<thead>
							<tr class="customer_list">
								<th>ID</th>
								<th>Customer Name</th>
								<th>Package Name</th>
								<th>Sim/Serial Number</th>
								<th>IMEI</th>
								<th>Recurring Price</th>
								<th>Valid</th>
								<th>Action</th>
								
							</tr>
						</thead>
						<tbody>
						    @foreach($rows as $key=>$row)
							<tr class="customer_list">
							   
							    <td >{{$key+1}}</td>
								<td>{{ $row->customer_name}}</td>
								<td>{{ $row->service_name??''}}</td>
								<td>{{ $row->sim}}</td>
								<td>{{ $row->imei}}</td>
								<td>${{ $row->service_monthly_price??0}}</td>
									
								<td > {{$row->created_at}} </td>
						
								<td><a href="/activation/request/update/{{$row->id}}" class="btn btn-success">Pay</a>
								<!--<a href="/customer/delete/{{$row->id}}"><i class="fa fa-trash btn btn-danger"></i></a>-->
								</td>
							
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection