<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>TAX INVOICE</title>

		<style>
			body{
			    background-image:url(/frontend/img/fsjgfhsgfhsd.PNG) no-repeat;;
			    /*background-position: center;*/
			     width: 100%;
                min-heigth: 500px;
			}
			.invoice-box {
				max-width: 800px;
				margin: auto;
				padding: 30px;
				border: 1px solid #eee;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
				font-size: 16px;
				line-height: 24px;
				font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
				color: #555;
			}

			.invoice-box table {
				width: 100%;
				line-height: inherit;
				text-align: left;
			}

			.invoice-box table td {
				padding: 5px;
				vertical-align: top;
			}

			.invoice-box table tr td:nth-child(2) {
				text-align: right;
			}

			.invoice-box table tr.top table td {
				/*padding-bottom: 20px;*/
			}

			.invoice-box table tr.top table td.title {
				font-size: 45px;
				line-height: 45px;
				color: #333;
			}

			.invoice-box table tr.information table td {
				/*padding-bottom: 40px;*/
				color:black;
			}

			.invoice-box table tr.heading td {
				background: #8CB6FC;
                color:white !important;
				border-bottom: 1px solid #ddd;
				color:black;
			}

			.invoice-box table tr.details td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.item td {
				border-bottom: 2px solid #000;
			}

			.invoice-box table tr.item.last td {
				border-bottom: none;
			}

			.invoice-box table tr.total td:nth-child(2) {
				border-top: 2px solid #eee;
				font-weight: bold;
			}

			@media only screen and (max-width: 600px) {
				.invoice-box table tr.top table td {
					width: 100%;
					display: block;
					text-align: center;
				}

				.invoice-box table tr.information table td {
					width: 100%;
					display: block;
					text-align: center;
				}
			}

			/** RTL **/
			.invoice-box.rtl {
				direction: rtl;
				font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
			}

			.invoice-box.rtl table {
				text-align: right;
			}

			.invoice-box.rtl table tr td:nth-child(2) {
				text-align: left;
			}
		
		</style>
	</head>

	<body >
		<div class="invoice-box">
			<table cellpadding="0" cellspacing="0">
				<tr class="top" style="padding-bottom:0px">
					<td colspan="7">
						<table>
							<tr>
							    <td>
                                    <img src="/assets/img/logo4.png" style="    width: 100%;max-width: 170px;margin-top: -22px;" /><br />
									<br />
								    <span style="font-size:16px;color:black">TAX INVOICE</span><br>
									<b style="color:black">Invoice #: </b><br /><br>
                                    <p style="font-size: 11px;
    margin-top: -10px;
    line-height: 20px;
    font-weight: 600;    margin-bottom: 0px;">
        WAREHOUSE 14B, OPP. AIKO MALL, STREET 59,<br />
DUBAI INVESTMENT PARK 1,<br />
DUBAI United Arab Emirates<br />
TRN 100458179700003<br />
    </p>
									
								</td>
								<td class="title">
									
    
    
    
    

								</td>

								
							</tr>
						</table>
					</td>
				</tr>


				

				<tr class="heading" style="background-color:#621aff">
				    <td style="width: 1%;text-align: center;">#</td>
					<td style="width: 25%;text-align: center;">SERVICE </td>
                    <td style="width: 20%;text-align: center;"> CLIENT</td>
                    <td style="width: 20%;text-align: center;"> SIM</td>
                    <td style="width: 20%;text-align: center;"> IMEI</td>
                    
                    <td style="width: 5%;text-align: right;">PRICE </td>
				</tr>
                @php
                $total=0;
                @endphp
                @foreach($data['rows'] as $key=>$row)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td style="text-align: center;">{{ $row->service_name }}</td>
                    <td style="text-align: center;">{{ $row->customer_name }}</td>
                    
                    <td style="text-align: center;">{{ $row->sim }}</td>
                    <td style="text-align: center;">{{ $row->imei }}</td>
                    <td style="text-align: right;">${{ $row->service_monthly_price }} @php $total=$total+$row->service_monthly_price @endphp</td>
                </tr>
                @endforeach
                <tr>
                    <br>
                </tr>
				<tr class="" style="margin-top:20px">
					<td colspan="3"></td>
                    <td colspan="3" style="text-align: right;color:black;font-weight:bold;background-color: lightgrey;">Total
					<td style="text-align: right;color:black;background-color: lightgrey;">AED{{ $total }}</td>
				</tr>
				<tr class="">
					<td colspan="3"></td>
                    <td colspan="3" style="text-align: right;color:black;font-weight:bold;border-bottom: 2px solid black;">Payment Made
					<td style="text-align: right;color:black;border-bottom: 2px solid black;">Offline </td>
				</tr>
			</table>
		</div>
	</body>
</html>