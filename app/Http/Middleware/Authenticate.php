<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Models\Notification;
use Auth;
class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if(Auth::user()){
            $notifications=Notification::where('for',Auth::user()->id)->where('is_seen',0)->get();
        Session::put('notifications',$notifications );
        }
        
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
