<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Setting;
class SettingController extends Controller
{

    public function mailServer()
    {
        $setting=Setting::where('portion','mail_server')->get();
        return view('admin.setting.mail_server',compact('setting'));
    }  
    public function mailServerPost($portion,Request $req){
        foreach($req->all() as $key=>$input){
            $exist=Setting::where('portion',$portion)->where('key',$key)->first();
            if(!$exist && $key !='_token'){
                $exist=new Setting();
                $exist->portion=$portion;
                $exist->key=$key;
                $exist->value=$input;
            $exist->save();
            }else{
                if($key !='_token'){
                    $exist->value=$input;
            $exist->save();
                }
                
            }
            
        }
        return redirect('/setting/'.$portion);
    }
    // public function store(Request $req){
    //     // dd($req->all());
    //     $user=User::create($req->all());
    //     $user->password=Hash::make($req->password);
    //     $user->save();
    //     return redirect('/user/'.$req->role.'/list');
    // }
    // public function edit($type,$id){
    //     $row=User::find($id);
    //     return view('admin.user.add',compact('row','type'));
    // }
    // public function update(Request $req,$type){
    //     $row=User::find($req->id);
    //     $row->first_name=$req->first_name;
    //     $row->email=$req->email;
    //     $row->last_name=$req->last_name;
    //     $row->business_name=$req->business_name;
    //     $row->status=$req->status;
    //     if($req->password){
    //         $user->password=Hash::make($req->password);
    //     }
    //     $row->save();
    //     return redirect('/user/'.$type.'/list');
    // }
    // public function delete($type,$id){
    //     $row=User::where('id',$id)->delete();
    //     return redirect('/user/'.$type.'/list');
    // }
      

}