<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Service;
use App\Models\Notification;
use App\Models\ServicePurchase;
use PDF;
class ServiceController extends Controller
{
    public function downloadInvoice(){
      
        $data['rows']=ServicePurchase::where('created_by',Auth::user()->id)->where('status','paid')->get();
        // return view('admin.service.pdf',compact('data'));
        $pdf = PDF::loadView('admin.service.pdf', $data);
        
        return $pdf->download('itsolutionstuff.pdf',compact('data'));
    }
    public function recurring(){
        $rows=ServicePurchase::where('created_by',Auth::user()->id)->where('status','paid')->get();
        $total=ServicePurchase::where('created_by',Auth::user()->id)->where('status','paid')->sum('service_monthly_price');
       return view('admin.service.recurring',compact('rows','total'));
    }
    public function activationRequestResponse($id,$status){
        $re=ServicePurchase::find($id);
        ServicePurchase::where('id',$id)->update([
            'status'=>$status]);
            Notification::create([
            'title'=>'Admin '.$status.' Your Request of #'. $re->id,
            'body'=>'Admin '.$status.' Your Request',
            'link'=>'/invoice/list',
            'for'=>$re->created_by,
            'created_by'=>1,
            'is_seen'=>0
            ]); 
        return redirect('/activation/requests')->with('success','Updated');
    }
    public function activationRequestUpdate($id){
        $row=ServicePurchase::find($id);
        $service=Service::find($row->service_id);
        return view('admin.service.activation-request',compact('row','service'));
    }
    public function invoiceList(){
       $rows=ServicePurchase::where('created_by',Auth::user()->id)->get();
       $payment='offline';
        return view('admin.customer.whole_sale_customers',compact('rows','payment'));
    }
    public function activationRequest(){
        $rows=ServicePurchase::where('status','pending')->get();
       return view('admin.customer.whole_sale_customers',compact('rows'));
    }
    public function purchaseService(Request $req){
        $service=Service::find($req->id);
        $request=ServicePurchase::create([
            'service_id'=>$req->id,
            'service_name'=>$service->name??'',
            'service_monthly_price'=>$service->price_per_month,
            'status'=>'Pending',
            'total'=>$service->price_per_month+$service->activation_fee,
            'activation_price'=>$service->activation_fee,
            'created_by'=>Auth::user()->id,
            'payment_method'=>Auth::user()->payment_method,
            'customer_name'=>$req->name??'',
            'sim'=>$req->sim,
            'imei'=>$req->imei??''
            ]);
        Notification::create([
            'title'=>'Request For '.$service->name,
            'body'=>Auth::user()->first_name.' Request for active service of '.$service->name,
            'link'=>'/service/request/'.$request->id,
            'for'=>1,
            'created_by'=>Auth::user()->id,
            'is_seen'=>0
            ]);   
            $notifications=Notification::where('for',Auth::user()->id)->where('is_seen',0)->get();
        Session::put('notifications',$notifications );
       return redirect('customer/list')->with('success','Request Successfully Submited.');
    }
    public function purchase($id){
        $service=Service::find($id);
        return view('admin.service.service-purchase',compact('service'));
    }
    public function list(Request $req)
    {
        $rows=Service::all();
        $filter=[];
        if(isset($req->cate) && $req->cate){
             $rows=Service::where('category_id',$req->cate)->get();
             $filter['cate']=$req->cate;
        }
           
        $categories=Category::where('type','Service')->where('parent_id',null)->get();
        return view('admin.service.list',compact('rows','categories','filter'));
    }  
    public function add(){
         $categories=Category::where('type','Service')->where('parent_id',null)->get();
        return view('admin.service.add',compact('categories'));
    }
    public function store(Request $req){
        Service::create($req->all());
            return redirect('/service/list');
    }
    public function edit($id){
        $row=Service::find($id);
        $categories=Category::where('type','Service')->where('parent_id',null)->get();
        return view('admin.service.add',compact('row','categories'));
    }
    public function update(Request $req){
        // dd($req->all());
        $row=Service::find($req->id);
        $row->name=$req->name;
        $row->status=$req->status;
        $row->category_id=$req->category_id;
        $row->equipment_name=$req->equipment_name;
        $row->equipment_price=$req->equipment_price;
        $row->activation_fee=$req->activation_fee;
        $row->price_per_month=$req->price_per_month;
        $row->renew_every_month=$req->renew_every_month;
        $row->client=$req->client??0;
        $row->dealer=$req->dealer??0;
        $row->sim=$req->sim??0;
        $row->imei=$req->imei??0;
        $row->wholesale_client=$req->wholesale_client??0;
        $row->save();
        return redirect('/service/list');
    }
    public function delete($id){
        $row=Service::where('id',$id)->delete();
        return redirect('/service/list');
    }

    public function listCate()
    {
        $rows=Category::where('type','Service')->get();
        $type="service";
        return view('admin.ticket.cate.list',compact('rows','type'));
    }  
    public function addCate(){
         $categories=Category::where('type','service')->get();
         $type="service";
        return view('admin.ticket.cate.add',compact('categories','type'));
    }
   
      

}