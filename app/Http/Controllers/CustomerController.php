<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Service;
use App\Models\Customer;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use DateTime;
use App\Models\ServicePurchase;
class CustomerController extends Controller
{

    public function list()
    {
       
        if(Auth::user()->role=='wholesale_client'){
            $rows=ServicePurchase::where('created_by',Auth::user()->id)->get();
            return view('admin.customer.whole_sale_customers',compact('rows'));
        }
        if(Auth::user()->role=='admin'){
            $rows=Customer::all();
        }else{
            $rows=Customer::where('created_by',Auth::user()->id)->get();
        }
        
        return view('admin.customer.list',compact('rows'));
    }  
    public function add(){
         $services=Service::all();
        return view('admin.customer.add',compact('services'));
    }
    public function store(Request $req){
        // dd($req->all());
        $customer=Customer::create($req->all());
        $customer->created_by=Auth::user()->id;
        $customer->save();
        // $service=Service::find($customer->plan_id);
        return view('admin.customer.customer-step2',compact('customer'));
    }
    public function paymentDone(Request $req){
        
        
    $customer=Customer::find($req->id);
        $token=$req->dataValue;
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName('34J7CvcYJ');
    $merchantAuthentication->setTransactionKey('2tLWF4F6vpe876tq');
    $refId = 'ref' . time();
    // Set the transaction's refId
    $opaqueData = new AnetAPI\OpaqueDataType();
    $opaqueData->setDataDescriptor("COMMON.ACCEPT.INAPP.PAYMENT");
    $opaqueData->setDataValue($token);
    
    
    $amount=$customer->service->price_per_month;
    $email=$customer->email;
    $name=$customer->name;



// Create the Bill To info for new payment type
    $billTo = new AnetAPI\CustomerAddressType();
    $billTo->setFirstName($name);
    $billTo->setLastName("");
    $billTo->setCompany($customer->business_name);
    $billTo->setAddress($customer->address);
    $billTo->setCity($customer->city);
    $billTo->setState($customer->state);
    $billTo->setZip($customer->zip);
    $billTo->setCountry("USA");
    $billTo->setPhoneNumber($customer->phone);
    $billTo->setfaxNumber("999-999-9999");

    // Create a customer shipping address
    $customerShippingAddress = new AnetAPI\CustomerAddressType();
    $customerShippingAddress->setFirstName($name);
    $customerShippingAddress->setLastName("");
    $customerShippingAddress->setCompany($customer->business_name);
    $customerShippingAddress->setAddress(rand() . $customer->address);
    $customerShippingAddress->setCity($customer->city);
    $customerShippingAddress->setState($customer->state);
    $customerShippingAddress->setZip($customer->zip);
    $customerShippingAddress->setCountry("USA");
    $customerShippingAddress->setPhoneNumber($customer->phone);
    $customerShippingAddress->setFaxNumber("999-999-9999");

    // Create an array of any shipping addresses
    $shippingProfiles[] = $customerShippingAddress;

//  $creditCard = new AnetAPI\CreditCardType();
//     $creditCard->setCardNumber("4242424242424242");
//     $creditCard->setExpirationDate("2038-12");
//     $creditCard->setCardCode("142");
//     $paymentCreditCard = new AnetAPI\PaymentType();
//     $paymentCreditCard->setCreditCard($creditCard);


     // Create a new CustomerPaymentProfile object
    $paymentProfile = new AnetAPI\CustomerPaymentProfileType();
    $paymentProfile->setCustomerType('individual');
    $paymentProfile->setBillTo($billTo);
    // $paymentProfile->setPayment($paymentCreditCard);
    $paymentProfiles[] = $paymentProfile;


    // Create a new CustomerProfileType and add the payment profile object
    $customerProfile = new AnetAPI\CustomerProfileType();
    $customerProfile->setDescription("add customer from dealer dashboard");
    $customerProfile->setMerchantCustomerId("M_" . time());
    $customerProfile->setEmail($email);
    // $customerProfile->setpaymentProfiles($paymentProfiles);
    $customerProfile->setShipToList($shippingProfiles);


    // Assemble the complete transaction request
    $request = new AnetAPI\CreateCustomerProfileRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setProfile($customerProfile);

    // Create the controller and get the response
    $controller = new AnetController\CreateCustomerProfileController($request);
    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

    $customerProfileId=$response->getCustomerProfileId();
    $customerAddressId=$response->getCustomerShippingAddressIdList()[0];
    // Assemble the complete transaction request
    $request = new AnetAPI\CreateCustomerProfileRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setProfile($customerProfile);
    
    $customerData = new AnetAPI\CustomerDataType();
    $customerData->setType("individual");
    $customerData->setId("99999456654");
    $customerData->setEmail($email);
    
    $paymentOne = new AnetAPI\PaymentType();
    $paymentOne->setOpaqueData($opaqueData);

    // Create order information
    $order = new AnetAPI\OrderType();
    $order->setInvoiceNumber("10101");
    $order->setDescription("Golf Shirts");
    
    
    // Add values for transaction settings
    $duplicateWindowSetting = new AnetAPI\SettingType();
    $duplicateWindowSetting->setSettingName("duplicateWindow");
    $duplicateWindowSetting->setSettingValue("60");

    // Add some merchant defined fields. These fields won't be stored with the transaction,
    // but will be echoed back in the response.
    $merchantDefinedField1 = new AnetAPI\UserFieldType();
    $merchantDefinedField1->setName("customerLoyaltyNum");
    $merchantDefinedField1->setValue("1128836273");

    $merchantDefinedField2 = new AnetAPI\UserFieldType();
    $merchantDefinedField2->setName("favoriteColor");
    $merchantDefinedField2->setValue("blue");

    // Create a TransactionRequestType object and add the previous objects to it
    $transactionRequestType = new AnetAPI\TransactionRequestType();
    $transactionRequestType->setTransactionType("authCaptureTransaction"); 
    $transactionRequestType->setAmount($amount);
    $transactionRequestType->setOrder($order);
    $transactionRequestType->setPayment($paymentOne);
    $transactionRequestType->setBillTo($billTo);
    $transactionRequestType->setCustomer($customerData);
    $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);
    $transactionRequestType->addToUserFields($merchantDefinedField1);
    $transactionRequestType->addToUserFields($merchantDefinedField2);

    // Assemble the complete transaction request
    $request = new AnetAPI\CreateTransactionRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setTransactionRequest($transactionRequestType);
    
    
    
    
    
    
    
    
    
    
    

    // Create the controller and get the response
    $controller = new AnetController\CreateCustomerProfileController($request);
    
    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
   
  
  
//   $customerProfileId='909200479';
  $customerPaymentProfileId='';
//   $customerAddressId='909863698';
  
//  $subscription = new AnetAPI\ARBSubscriptionType();
//     $subscription->setName("Sample Subscription");

//     $interval = new AnetAPI\PaymentScheduleType\IntervalAType();
//     $interval->setLength(7);
//     $interval->setUnit("days");

//     $paymentSchedule = new AnetAPI\PaymentScheduleType();
//     $paymentSchedule->setInterval($interval);
//     $paymentSchedule->setStartDate(new DateTime('2023-02-20'));
//     $paymentSchedule->setTotalOccurrences("12");
//     $paymentSchedule->setTrialOccurrences("1");

//     $subscription->setPaymentSchedule($paymentSchedule);
//     $subscription->setAmount(rand(1,99999)/12.0*12);
//     $subscription->setTrialAmount("0.00");
    
//     $profile = new AnetAPI\CustomerProfileIdType();
//     $profile->setCustomerProfileId($customerProfileId);
//     $profile->setCustomerPaymentProfileId($customerPaymentProfileId);
//     $profile->setCustomerAddressId($customerAddressId);

//     $subscription->setProfile($profile);

//     $request = new AnetAPI\ARBCreateSubscriptionRequest();
//     $request->setmerchantAuthentication($merchantAuthentication);
//     $request->setRefId($refId);
//     $request->setSubscription($subscription);
//     $controller = new AnetController\ARBCreateSubscriptionController($request);

//     $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
    
    
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
        return redirect('/index')->with('success','Payment Successfully Done');
        
        
        
        echo "Succesfully created customer profile : " . $response->getCustomerProfileId() . "\n";
        $paymentProfiles = $response->getCustomerPaymentProfileIdList();
        echo "SUCCESS: PAYMENT PROFILE ID : " . $paymentProfiles[0] . "\n";
    } else {
        echo "ERROR :  Invalid response\n";
        $errorMessages = $response->getMessages()->getMessage();
        echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
    }
    return $response;
        
        
        
        
    }
    public function customerStep2(Request $req){
        $row=Customer::find($req->id);
        $row->business_name=$req->business_name;
        $row->phone=$req->phone;
        $row->city=$req->city;
        $row->country=$req->country;
        $row->address=$req->address;
        $row->zip=$req->zip;
        $row->save();
        return view('admin.customer.customer-step3',compact('row'));
    }
    public function edit($id){
        $row=Customer::find($id);
        $services=Service::all();
        return view('admin.customer.add',compact('row','services'));
    }
    public function update(Request $req){
        $row=Customer::find($req->id);
        $row->name=$req->name;
        $row->email=$req->email;
        $row->imei=$req->imei;
        $row->sim=$req->sim;
        $row->status=$req->status;
        $row->service_id=$req->service_id;
        $row->save();
        return redirect('/customer/list');
    }
    public function delete($id){
        $row=Customer::where('id',$id)->delete();
        return redirect('/customer/list');
    }
      

}