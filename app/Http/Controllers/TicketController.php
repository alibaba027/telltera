<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Ticket;
use App\Models\Reply;
class TicketController extends Controller
{
    public function list(){
        if(Auth::user()->role=='admin'){
            $tickets=Ticket::orderBy('created_at','desc')->get();
        }else{
            $tickets=Ticket::where('created_by',Auth::user()->id)->orderBy('created_at','desc')->get();
        }
        return view('admin.ticket.list',compact('tickets'));
    }
    public function reply(Request $req){
        $imageName='';
        if($req->image){
            $imageName = time().'.'.$req->image->extension();  
            $req->image->move(public_path('images'), $imageName);
        }
     if($req->deacription){
         Reply::create([
            'ticket_id'=>$req->id,
            'reply'=>$req->deacription,
            'image'=>$imageName,
            'reply_name'=>Auth::user()->first_name .' '. Auth::user()->last_name,
            'created_by'=>Auth::user()->id
            ]);
     }
        
            if(Auth::user()->role=='admin'){
               
                Ticket::where('id',$req->id)->update([
                'status'=>$req->status
                ]);
            }
            
            $tickets=Ticket::where('status','in progress')->count();
        Session::put('tickets',$tickets );
         return redirect('/ticket/list')->with('success','submited');        
      
    }
    public function view($id){
         $ticket=Ticket::find($id);
         return view('admin.ticket.view',compact('ticket'));
    }
    public function add(){
        $categories=Category::where('type','Ticket')->get();
       
        return view('admin.ticket.add',compact('categories'));
    }
    public function store(Request $req){
     
        $imageName='';
        if($req->image){
            $imageName = time().'.'.$req->image->extension();  
            $req->image->move(public_path('images'), $imageName);
        }
        Ticket::create([
            'title'=>$req->title,
            'body'=>$req->deacription,
            'category_id'=>$req->category_id,
            'image'=>$imageName,
            'status'=>'in progress',
            'created_by'=>Auth::user()->id
            ]);
             $tickets=Ticket::where('status','in progress')->count();
        Session::put('tickets',$tickets );
        return redirect('/ticket/list')->with('success','submited');
    }
    public function cateList()
    {
        $rows=Category::where('type','Ticket')->get();
        return view('admin.ticket.cate.list',compact('rows'));
    }  
    public function cateAdd(){
        return view('admin.ticket.cate.add');
    }
    public function cateStore(Request $req){
        Category::create([
            'name'=>$req->name,
            'status'=>$req->status,
            'type'=>$req->type??'Ticket',
            'parent_id'=>$req->parent_id??null
            ]);
            return back();
    }
    public function cateEdit($id){
        $row=Category::find($id);
        $categories=Category::where('type',$row->type)->get();
        return view('admin.ticket.cate.add',compact('row','categories'));
    }
    public function cateUpdate(Request $req){
        $row=Category::find($req->id);
        $row->name=$req->name;
        $row->status=$req->status;
        $row->parent_id=$req->parent_id??null;
        $row->save();
        if($row->type=='Service'){
            return redirect('/service/category/list');
        }else{
            return redirect('/category/list');
        }
    }
    public function deleteCate($id){
        $row=Category::find($id);
        Category::where('id',$id)->delete();
        if($row->type=='Service'){
            return redirect('/service/category/list');
        }else{
            return redirect('/category/list');
        }
        
    }
      

}