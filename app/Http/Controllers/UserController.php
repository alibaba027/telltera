<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Service;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
class UserController extends Controller
{
    public function showPaymentId($id){
        return view('admin.user.add-card-payment-id',compact('id'));
    }
    public function StorePaymentId(Request $request){
        $user=User::find($request->id);
        $user->paymentprofileid=$request->payment_id;
        $user->save();
        return redirect('/user/client/list');
    }
    public function addCard($id){
        return view('admin.user.add-card',compact('id'));
    }
    public function createUser(Request $request){
        // dd($request->all());
    $user=User::find($request->id);
    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
    $merchantAuthentication->setName(env('MERCHANT_LOGIN_ID'));
    $merchantAuthentication->setTransactionKey(env('MERCHANT_TRANSACTION_KEY'));
    
    // Set the transaction's refId
    $refId = 'ref' . time();

    // Create a Customer Profile Request
    //  1. (Optionally) create a Payment Profile
    //  2. (Optionally) create a Shipping Profile
    //  3. Create a Customer Profile (or specify an existing profile)
    //  4. Submit a CreateCustomerProfile Request
    //  5. Validate Profile ID returned

    // Set credit card information for payment profile
    $creditCard = new AnetAPI\CreditCardType();
    // $creditCard->setCardNumber($request->card_number);
    // $creditCard->setExpirationDate($request->year.'-'.$request->month);
    // $creditCard->setCardCode($request->cvc);
    // $paymentCreditCard = new AnetAPI\PaymentType();
    // $paymentCreditCard->setCreditCard($creditCard);

    // Create the Bill To info for new payment type
    $billTo = new AnetAPI\CustomerAddressType();
    $billTo->setFirstName($user->first_name);
    $billTo->setLastName($user->last_name);
    $billTo->setCompany($user->business_name);
    $billTo->setAddress($request->address);
    $billTo->setCity($request->city);
    $billTo->setState($request->state);
    $billTo->setZip($request->zip);
    $billTo->setCountry($request->country);
    $billTo->setPhoneNumber($request->phone);
    $billTo->setfaxNumber("999-999-9999");

    // Create a customer shipping address
    $customerShippingAddress = new AnetAPI\CustomerAddressType();
    $customerShippingAddress->setFirstName($user->first_name);
    $customerShippingAddress->setLastName($user->last_name);
    $customerShippingAddress->setCompany($user->business_name);
    $customerShippingAddress->setAddress($request->address);
    $customerShippingAddress->setCity($request->city);
    $customerShippingAddress->setState($request->state);
    $customerShippingAddress->setZip($request->zip);
    $customerShippingAddress->setCountry($request->country);
    $customerShippingAddress->setPhoneNumber($request->phone);
    $customerShippingAddress->setFaxNumber("999-999-9999");

    // Create an array of any shipping addresses
    $shippingProfiles[] = $customerShippingAddress;


    // Create a new CustomerPaymentProfile object
    $paymentProfile = new AnetAPI\CustomerPaymentProfileType();
    $paymentProfile->setCustomerType('individual');
    $paymentProfile->setBillTo($billTo);
    // $paymentProfile->setPayment($paymentCreditCard);
    $paymentProfiles[] = $paymentProfile;


    // Create a new CustomerProfileType and add the payment profile object
    $customerProfile = new AnetAPI\CustomerProfileType();
    $customerProfile->setDescription("add customer in authoroze");
    $customerProfile->setMerchantCustomerId("M_" . time());
    $customerProfile->setEmail($user->email);
    // $customerProfile->setpaymentProfiles($paymentProfiles);
    $customerProfile->setShipToList($shippingProfiles);


    // Assemble the complete transaction request
    $request = new AnetAPI\CreateCustomerProfileRequest();
    $request->setMerchantAuthentication($merchantAuthentication);
    $request->setRefId($refId);
    $request->setProfile($customerProfile);

    // Create the controller and get the response
    $controller = new AnetController\CreateCustomerProfileController($request);
    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
  
    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
        $user->profileid=$response->getCustomerProfileId();
        $paymentProfiles = $response->getCustomerPaymentProfileIdList();
        // $user->paymentprofileid=$paymentProfiles[0];
        $user->save();
        return redirect('/user/client/list');
        dd($response);
        echo "Succesfully created customer profile : " . $response->getCustomerProfileId() . "\n";
        $paymentProfiles = $response->getCustomerPaymentProfileIdList();
        echo "SUCCESS: PAYMENT PROFILE ID : " . $paymentProfiles[0] . "\n";
    } else {
        echo "ERROR :  Invalid response\n";
        $errorMessages = $response->getMessages()->getMessage();
        echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
    }
    return $response;
    }
    public function list($type)
    {
        if(Auth::user()->role=='admin'){
         $rows=User::where('role',$type)->orderBy('created_at','desc')->get();
        }else{
            $rows=User::where('created_at',Auth::user()->id)->where('role',$type)->orderBy('created_at','desc')->get();
        }
        return view('admin.user.list',compact('rows','type'));
    }  
    public function add($type){
        return view('admin.user.add',compact('type'));
    }
    public function store(Request $req){
        // dd($req->all());
        $user=User::create($req->all());
        $user->password=Hash::make($req->password);
        $user->created_by=Auth::user()->id;
        $user->payment_method=$req->payment_method;
        $user->commission_percentage=$req->commission_percentage;
        $user->fixed_percentage=$req->fixed_percentage;
        if($req->role=='client'){
             $user->role='wholesale_client';
        }
        $user->save();
        return redirect('/user/'.$req->role.'/list');
    }
    public function edit($type,$id){
        $row=User::find($id);
        return view('admin.user.add',compact('row','type'));
    }
    public function update(Request $req,$type){
        $row=User::find($req->id);
        $row->first_name=$req->first_name;
        $row->email=$req->email;
        $row->last_name=$req->last_name;
        $row->business_name=$req->business_name;
        $row->status=$req->status;
        $row->payment_method=$req->payment_method;
        $row->commission_percentage=$req->commission_percentage;
        $row->fixed_percentage=$req->fixed_percentage;
        if($req->password){
            $row->password=Hash::make($req->password);
        }
        $row->save();
        return redirect('/user/'.$type.'/list');
    }
    public function delete($type,$id){
        $row=User::where('id',$id)->delete();
        return redirect('/user/'.$type.'/list');
    }
    public function getProfile(){
        return view('admin.user.profile');
    }  
    public function updateProfile(Request $req){
        $user=User::where('id',$req->id)->first();
        $user->first_name=$req->first_name;
        $user->last_name=$req->last_name;
        $user->business_name=$req->business_name;
         if($req->password){
            $user->password=Hash::make($req->password);
        }
        $user->save();
        return redirect('/profile')->with('success','Updated Profile');
    }

}