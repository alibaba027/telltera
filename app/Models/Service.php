<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Service extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'status',
        'category_id',
        'activation_fee',
        'price_per_month',
        'equipment_name',
        'equipment_price',
        'client',
        'dealer',
        'wholesale_client',
        'imei',
        'sim'
    ];
    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }
    
    
}
