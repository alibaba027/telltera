<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class ServicePurchase extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'service_id',
        'service_name',
        'service_monthly_price',
        'status',
        'total',
        'activation_price',
        'created_by',
        'payment_method',
        'customer_name',
        'sim',
        'imei'
    ];
    public function service(){
        return $this->hasOne(Service::class,'id','service_id');
    }
    public function client(){
        return $this->hasOne(User::class,'id','created_by');
    }
    
    
}
